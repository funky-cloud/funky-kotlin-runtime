#!/bin/bash

PORT=9099 LANG="kotlin" \
FUNCTION_CODE="$(cat ./samples/hello/index.kt)" \
java -jar ./target/funky-kotlin-runtime-1.0.0-SNAPSHOT-fat.jar & echo $!

FUNCTION_ID=$!
echo "🚀 function with id:$FUNCTION_ID is started and is listening on http port: $PORT"

sleep 600
kill -9 FUNCTION_ID
