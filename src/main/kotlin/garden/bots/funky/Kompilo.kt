package garden.bots.funky

import io.vertx.ext.web.RoutingContext
import javax.script.Invocable
import javax.script.ScriptEngineManager

class Kompilo {

  //private val engine = ScriptEngineManager().getEngineByExtension("kts")!!
  private val engine = ScriptEngineManager().getEngineByExtension("kts")!!

  private val invoker = engine as Invocable

  /*
  fun compileFunction(functionCode: String, language: String) : Result<Any> {
    return try {
      Result.success(engine.eval(functionCode))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }
   */

  fun compileFunction(functionCode: String) : Result<Any> {

    return try {
      Result.success(engine.eval(functionCode))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }

  fun invokeFunction(name: String?, params: Any) : Result<Any> {
    return try {
      Result.success(invoker.invokeFunction(name, params))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }

  fun invokeFunction(name: String?, params: Any, context: RoutingContext) : Result<Any> {
    return try {
      Result.success(invoker.invokeFunction(name, params, context))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }

  fun invokeFunction(name: String?) : Result<Any> {
    return try {
      Result.success(invoker.invokeFunction(name))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }


}
